# Mainmenu CSM

Minetest mod that adds a client-side mod (CSM) tab to the main menu.

The CSM tab was created by oilboi a.k.a jordan4ibanez.

See <https://github.com/minetest/minetest/pull/10039>