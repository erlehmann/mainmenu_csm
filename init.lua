if not minetest.is_singleplayer() then
	error("Main menu changes work only in singleplayer mode.")
end

local current_main_menu_path = minetest.settings:get("main_menu_path")
local main_menu_path = minetest.get_modpath("mainmenu_csm") .. '/builtin/mainmenu'
if current_main_menu_path ~= main_menu_path then
	minetest.register_on_joinplayer(
		function()
			minetest.settings:set("main_menu_path", main_menu_path)
			minetest.kick_player(
				"singleplayer",
				[[
CSM tab added to main menu.

Reconnect to play the game.
]]
			)
		end
	)
end
